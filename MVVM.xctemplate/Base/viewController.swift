//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Template created by Alvaro Royo.
//  Linkedin: https://www.linkedin.com/in/alvaroroyo
//
//  ___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___: UIViewController {
    
    var viewModel: ___VARIABLE_productName___ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Your code here
    }
    
}

//MARK: - ViewModel communication
protocol ___FILEBASENAME___Protocol: AnyObject {
    
}

extension ___FILEBASENAME___: ___FILEBASENAME___Protocol {
    
}
