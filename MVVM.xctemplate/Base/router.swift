//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Template created by Alvaro Royo.
//  Linkedin: https://www.linkedin.com/in/alvaroroyo
//
//  ___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___ {
    
    weak var viewController: ___VARIABLE_productName___ViewController?
    
    static func getViewController() -> ___VARIABLE_productName___ViewController {
        
        let configuration = configureModule()
        
        return configuration.vc
        
    }
    
}

//MARK: - MVVM
extension ___FILEBASENAME___ {
    
    private static func configureModule() -> (vc: ___VARIABLE_productName___ViewController, vm: ___VARIABLE_productName___ViewModel, rt: ___VARIABLE_productName___Router) {
        
        let viewController = ___VARIABLE_productName___ViewController()
        let router = ___VARIABLE_productName___Router()
        let viewModel = ___VARIABLE_productName___ViewModel()
        
        viewController.viewModel = viewModel
        
        viewModel.router = router
        viewModel.view = viewController
        
        router.viewController = viewController
        
        return (viewController, viewModel, router)
        
    }
    
}
