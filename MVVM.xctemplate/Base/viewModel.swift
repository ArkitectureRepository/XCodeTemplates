//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Template created by Alvaro Royo.
//  Linkedin: https://www.linkedin.com/in/alvaroroyo
//
//  ___COPYRIGHT___
//

import Foundation

final class ___FILEBASENAME___ {
    
    weak var view: ___VARIABLE_productName___ViewControllerProtocol?
    var router: ___VARIABLE_productName___Router?
    
}
