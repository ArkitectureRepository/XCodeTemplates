//: Playground - noun: a place where people can play

import Foundation

enum MobilePlattform : String {
    case ios = "\"[key]\" = \"[value]\";"
    case android = "<string name=\"[key]\">[value]</string>"
    case web = "[key]=>[value]"
}

var plattform:MobilePlattform = .ios
//plattform = .ios

let keys:String = #"""
wo1_title
wo1_subtitle
wo1_subtitle_start
wo2_title
wo2_subtitle
po1_title
po1_subtitle
po2_title
po2_subtitle
welcome_facebook_btn
welcome_google_btn
welcome_email_btn
onboarding_email_title
onboarding_email_placeholder
onboarding_password_title
onboarding_password_placeholder
onboarding_city_title
onboarding_city_tip
onboarding_type_title
onboarding_type_client
onboarding_type_professional
onboarding_type_tip
onboarding_phone_title
onboarding_phone_placeholder
onboarding_phone_tip
onboarding_data_title
onboarding_data_name_placeholder
onboarding_data_password_placeholder
onboarding_emailValidation_title
onboarding_emailValidation_mailBtn
onboarding_emailValidation_resendBtn
onboarding_resendEmail_alert_title
onboarding_resendEmail_alert_subtitle
onboarding_resetpass_title
onboarding_resetEmail_title
"""#

let values:String = #"""
¡Bienvenido a timbrit!
%@ Encuentra los mejores profesionales del hogar. Haz tu primera solicitud, chatea con profesionales, elige la mejor cotización y valóralo.
Si eres cliente...
Si eres profesional
Regístrate, completa tu perfil profesional, recibe solicitudes, chatea con clientes, envía cotizaciones ¡cierra el trabajo!
Las notificaciones son muy importantes :)
Te avisaremos cuando alguien te contacte y otras notificaciones importantes
Necesitaremos permiso de geolocalización
Así garantizamos que profesionales y clientes puedan contactarse
Continuar con Facebook
Continuar con Google
Continuar con mi email
Regístrate o inicia sesión con tu cuenta de email
Escribe tu email
Reconocemos este email\n¡Nos alegra verte de vuelta!
Ingresa tu contraseña
Selecciona tu ciudad
Por ahora estamos en las ciudades que indicamos arriba
¡Dinos quién eres!
Soy\ncliente
Soy\nprofesional
Decide si eres cliente o profesional
Ingresa tu número de teléfono celular
Número de teléfono
Esta información nos ayudará a brindarte un mejor servicio
¡Falta poco! Completa los siguientes datos y sube una foto de perfil para crear tu cuenta
Nombre y apellido
Contraseña
¡Revisa tu correo!\nTe enviamos un email para confirmar tu cuenta
Ir al correo electrónico
No me llegó, enviar de nuevo
¡Te re-enviamos el email de confirmación!
Chequea tu email y valida tu cuenta para poder ingresar a timbrit, solo te tomará un minuto
Te enviaremos un email de recuperación de tu clave a la siguiente dirección:
Este es el email donde enviamos la verificación. Si deseas puedes modificarlo:
"""#

let keysArray = keys.split(separator: "\n")
let valuesArray = values.split(separator: "\n")

if keysArray.count == valuesArray.count {
    for i in 0..<keysArray.count {
        let key = String.init(keysArray[i])
        let value = String.init(valuesArray[i])
        //Adaptación a compilador online:
        var totalString = plattform.rawValue.replacingOccurrences(of: "[key]", with: key)
        totalString = totalString.replacingOccurrences(of: "[value]", with: value)
        print(totalString)
    }
}else{
    print("No hay la misma cantidad de keys que de valores")
}
