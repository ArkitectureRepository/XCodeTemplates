#!/bin/sh

MVVM=$PWD/MVVM.xctemplate
UIXibView=$PWD/UIXibView.xctemplate
SNIPPETS=$PWD/CodeSnippets
THEMES=$PWD/FontAndColorThemes

cp -a "$MVVM" /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/File\ Templates/Source
cp -a "$UIXibView" /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/File\ Templates/Source
cp -a "$SNIPPETS" ~/Library/Developer/Xcode/UserData
cp -a "$THEMES" ~/Library/Developer/Xcode/UserData

kill $(ps aux | grep 'Xcode' | awk '{print $2}')
