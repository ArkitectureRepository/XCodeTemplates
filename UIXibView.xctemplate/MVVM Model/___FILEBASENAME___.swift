//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Template created by Alvaro Royo.
//  Linkedin: https://www.linkedin.com/in/alvaroroyo
//
//  ___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___ : UIXibView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Code here!
    }
    
}
