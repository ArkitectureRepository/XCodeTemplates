# Custom XCode Class Templates

## How to install

You can install the templates folder in XCode.<br /><br />
Open a terminal and go into the XCodeTemplates folder<br />
Then execute command: sudo sh ./help.sh<br />
Restart XCode<br /><br />
Go to applications<br />
Then go to XCode inside the package.<br />
Contents -> Developer -> Platforms -> iPhoneOS.platform -> Developer -> Library -> Xcode -> Templates -> File Templates -> Source<br />
The copy "Custom.xctemplate" inside<br />
Restart XCode<br />
In you proyect when create new file you can select Custom template<br />
Enjoy!
